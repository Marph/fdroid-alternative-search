"""
Webapp, providing an alternative search functionality for the F-Droid repo.
"""

from datetime import datetime
import functools
import json
import operator
import urllib.parse

from flask import Flask, request, render_template
import pandas as pd


APP = Flask(__name__)
FDROID_APPS = []  # Global, because it's updated in the background.
FDROID_APPS_DF = pd.DataFrame()
CONFIGURATION = {}
LAST_DATA_UPDATE = None


def update_apps():
    print("Started downloading the fdroid package index.")
    global FDROID_APPS
    with open("index-v1.json") as infile:
        FDROID_APPS = json.load(infile)["apps"]
    print("Finished downloading the fdroid package index.")


def obtain_configuration():
    print("Started obtaining the configuration.")
    global CONFIGURATION
    # get unique values for user filtering and further processing
    app_infos = {
        "anti_features": set(),
        "categories": set(),
        "hosts": set(),
        "licenses": set(),
    }
    for fdroid_app in FDROID_APPS:
        # print(fdroid_app["id"])
        app_infos["anti_features"].update(fdroid_app.get("antiFeatures", ()))
        app_infos["categories"].update(fdroid_app["categories"])
        app_infos["licenses"].add(fdroid_app["license"])
        if (source_url := fdroid_app.get("sourceCode")) is not None:
            host = urllib.parse.urlparse(source_url).netloc
            app_infos["hosts"].add(host)
    assert (
        len(app_infos["anti_features"] & app_infos["categories"]) == 0
    ), "duplicated entries"
    # sets are not sortable: https://stackoverflow.com/a/55389242/7410886
    CONFIGURATION = {key: sorted(list(value)) for key, value in app_infos.items()}
    print("Finished obtaining the configuration.")


def get_summary(app) -> str:
    try:
        return app["summary"]
    except KeyError:
        for localization in ("en-US", "en_US", "en-GB", "en"):
            try:
                return app["localized"][localization]["summary"]
            except KeyError:
                pass
    return app.get("description", "-")  # last resort


def prepare_data():
    # Running locally means debugging.
    debug = request.url_root == "http://127.0.0.1:5000/"
    print("Started preparing the data.")
    global FDROID_APPS_DF
    fdroid_apps_updated = []
    # put the data in a dataframe:
    # create a separate binary column for each option
    for i, fdroid_app in enumerate(FDROID_APPS[:100] if debug else FDROID_APPS):
        print(f"Processed {i} apps", end="\r", flush=True)
        # print(fdroid_app)
        if "sourceCode" not in fdroid_app:
            continue  # TODO
        anti_features = {af: False for af in CONFIGURATION["anti_features"]}
        for anti_feature in fdroid_app.get("antiFeatures", ()):
            anti_features[anti_feature] = True
        categories = {af: False for af in CONFIGURATION["categories"]}
        for category in fdroid_app.get("categories", ()):
            categories[category] = True

        # Hard reset monthly clicks at first day of the month.
        # TODO: Sliding window or even configurable.
        if datetime.today().day == 1:
            clicks = 0
        else:
            try:
                clicks = FDROID_APPS_DF.loc[
                    FDROID_APPS_DF["id"] == fdroid_app["packageName"], ["clicks"]
                ][0]
            except KeyError:
                clicks = 0

        fdroid_apps_updated.append(
            {
                **anti_features,
                **categories,
                "id": fdroid_app["packageName"],
                "host": urllib.parse.urlparse(fdroid_app["sourceCode"]).netloc,
                "last_updated": fdroid_app["lastUpdated"],
                "license": fdroid_app["license"],
                "name": (
                    fdroid_app.get("name") or fdroid_app["localized"]["en-US"]["name"]
                ).strip(),
                "summary": get_summary(fdroid_app),
                "clicks": clicks,
            }
        )
    fdroid_apps_updated = pd.DataFrame(fdroid_apps_updated)
    print("\n")
    # convert types
    for column in CONFIGURATION["anti_features"] + CONFIGURATION["categories"]:
        fdroid_apps_updated[column] = fdroid_apps_updated[column].astype(bool)
    fdroid_apps_updated["clicks"] = fdroid_apps_updated["clicks"].astype(int)
    fdroid_apps_updated["last_updated"] = pd.to_datetime(
        fdroid_apps_updated["last_updated"], unit="ms"
    )
    # sort alphabetically (once per day, not at every request)
    # TODO: Handle all names as lower case.
    fdroid_apps_updated.sort_values(
        ["clicks", "name"], ascending=[False, True], inplace=True
    )
    FDROID_APPS_DF = fdroid_apps_updated
    print("Finished preparing the data.")


@APP.route("/update")
def update():
    update_apps()
    obtain_configuration()
    prepare_data()
    global LAST_DATA_UPDATE
    LAST_DATA_UPDATE = datetime.now()
    return ("", 204)


def conjunction(conditions):
    # TODO: np.logical_and or operator.__and__?
    return functools.reduce(operator.__and__, conditions)


@APP.route("/search", methods=["POST"])
def search():
    filters = request.form.to_dict()

    if not filters["lastUpdateMin"]:
        last_update_min = None
    else:
        try:
            last_update_min = datetime.strptime(filters["lastUpdateMin"], "%Y-%m-%d")
        except ValueError:
            return f"Invalid Date format: {filters['lastUpdateMin']}"

    # create filter conditions
    filter_conditions = [
        FDROID_APPS_DF["name"].str.lower().str.contains(filters["search"].lower())
    ]
    licences_include = []
    licences_exclude = []
    hosts_include = []
    hosts_exclude = []
    for name, value in filters.items():
        if not name.startswith("select-"):
            continue
        # This assumes that there is no "-" in the filter category!
        _, filter_category, filter_name = name.split("-", 2)
        # "license" and "host" is a field with a single value,
        # while the other filters were splitted into booleans.
        if value == "dont-care":
            if filter_category == "Hosts":
                hosts_include.append(filter_name)
            elif filter_category == "Licenses":
                licences_include.append(filter_name)
            continue
        if value == "include":
            if filter_category == "Hosts":
                hosts_include.append(filter_name)
            elif filter_category == "Licenses":
                licences_include.append(filter_name)
            else:
                filter_conditions.append(FDROID_APPS_DF[filter_name])
        else:
            if filter_category == "Hosts":
                hosts_exclude.append(filter_name)
            elif filter_category == "Licenses":
                licences_exclude.append(filter_name)
            else:
                filter_conditions.append(~FDROID_APPS_DF[filter_name])

    if last_update_min is not None:
        filter_conditions.append(
            FDROID_APPS_DF["last_updated"]
            > datetime.strptime(filters["lastUpdateMin"], "%Y-%m-%d")
        )
    if licences_include:
        filter_conditions.append(FDROID_APPS_DF["license"].isin(licences_include))
    if hosts_include:
        filter_conditions.append(FDROID_APPS_DF["host"].isin(hosts_include))

    df_filtered = FDROID_APPS_DF[conjunction(filter_conditions)]
    df_filtered_json = json.loads(
        df_filtered[["name", "id", "summary"]].to_json(orient="records")
    )
    return render_template("app_table.html", apps=df_filtered_json)


@APP.route("/increment_clicks")
def increment_clicks():
    app_id = request.args.to_dict().get("id")
    if app_id is None:
        print(f"{app_id=}")
    else:
        global FDROID_APPS_DF
        FDROID_APPS_DF.loc[FDROID_APPS_DF["id"] == app_id, "clicks"] += 1
    return ("", 200)


@APP.route("/")
@APP.route("/index.html")
def index():
    if LAST_DATA_UPDATE is None:
        return "No data available, yet."
    return render_template(
        "index.html",
        configuration=CONFIGURATION,
        last_data_update=LAST_DATA_UPDATE.strftime("%Y-%m-%d, %H:%M:%S"),
    )


def main():
    # for local debugging
    APP.run(debug=True)


if __name__ == "__main__":
    main()
