import io
from zipfile import ZipFile

import requests


def download_index():
    """Download a new index."""
    response = requests.get("https://f-droid.org/repo/index-v1.jar")
    with ZipFile(io.BytesIO(response.content), "r") as inzip:
        inzip.extract("index-v1.json")


def trigger_data_update(host: str):
    """Trigger the update on the webapp."""
    response = requests.get(f"{host}/update")
    response.raise_for_status()
