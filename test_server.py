"""
Short local test for the webserver.
"""

from pathlib import Path

import requests

from common import download_index, trigger_data_update


if not Path("index-v1.json").is_file():
    print("Download a new index.")
    download_index()

HOST = "http://127.0.0.1:5000"

print("Trigger first update on the webapp.")
trigger_data_update(HOST)

print("Simulate click on link.")
requests.get(f"{HOST}/increment_clicks?id=de.j4velin.systemappmover")

# Trigger the update on the webapp for a second time.
# I. e. check if the real update works.
print("Trigger second update on the webapp.")
trigger_data_update(HOST)
