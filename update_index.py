"""
Update the F-Droid package index and trigger the corresponding webapp endpoint.
Should be executed in regular interval.
"""

from common import download_index, trigger_data_update


if __name__ == "__main__":
    download_index()
    trigger_data_update("https://fdroidsearch.eu.pythonanywhere.com")
